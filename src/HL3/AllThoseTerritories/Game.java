package HL3.AllThoseTerritories;

import HL3.AllThoseTerritories.model.*;
import javafx.beans.binding.MapBinding;
import javafx.collections.ObservableMap;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game {
    private Player[] players;
    private List<Patch> patches;
    private Map<String, Territory> territories;
    private Map<String, Continent> continents;
    private int currentPlayer;

    public Game(){
        players = new Player[2];
        players[0] = new Player(new Color(255, 0, 0), true);
        players[1] = new Player(new Color(0, 0, 255), false);
        patches = new ArrayList<Patch>();
        territories = new HashMap<String, Territory>();
        continents = new HashMap<String, Continent>();
        currentPlayer = 0;
    }

    public void setup(String filename){
        MapLoader.load(filename, patches, territories, continents);
    }

    public static void main(String[] args) {
	    Game mainGame = new Game();
        mainGame.setup(args[0]);
    }
}
