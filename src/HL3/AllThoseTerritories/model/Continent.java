package HL3.AllThoseTerritories.model;

import java.util.List;

/**
 * Created by lukas on 06/01/16.
 */
public class Continent {
    private String name;
    private int bonus;
    private List<Territory> territories;

    public Continent(String name, int bonus, List<Territory> territories){
        this.name = name;
        this.bonus = bonus;
        this.territories = territories;
    }
}
