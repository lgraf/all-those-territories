package HL3.AllThoseTerritories.model;

import java.awt.*;

/**
 * Created by lukas on 06/01/16.
 */
public class Player {
    private Color color;
    private boolean isHuman;

    public Player(Color color, boolean isHuman){
        this.color = color;
        this.isHuman = isHuman;
    }
}
