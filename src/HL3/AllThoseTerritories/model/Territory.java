package HL3.AllThoseTerritories.model;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lukas on 06/01/16.
 */
public class Territory {
    private String name;
    private Point capital;
    private int armysize;
    private Player owner;
    private List<Patch> patches;
    private List<Territory> neighbours;

    public Territory(String name){
        this.name = name;
        patches = new ArrayList<Patch>();
        neighbours = new ArrayList<Territory>();
    }

    public void setCapital(Point capital){
        this.capital = capital;
    }

    public void setArmysize(int armysize){
        this.armysize = armysize;
    }

    public void setOwner(Player owner){
        this.owner = owner;
    }

    public void addPatch(Patch newPatch){
        patches.add(newPatch);
    }

    public void setNeighbours(List<Territory> neighbours){
        this.neighbours = neighbours;
    }
}
